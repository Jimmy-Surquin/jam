<?php /* Smarty version Smarty-3.1-DEV, created on 2016-09-12 16:55:45
         compiled from "module_file_tpl:News;articlelist.tpl" */ ?>
<?php /*%%SmartyHeaderCode:96655323357d6c1f16303c1-86006246%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '60befe20a75d407574fe658fe037d1dc98318fd0' => 
    array (
      0 => 'module_file_tpl:News;articlelist.tpl',
      1 => 1473692082,
      2 => 'module_file_tpl',
    ),
  ),
  'nocache_hash' => '96655323357d6c1f16303c1-86006246',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'mod' => 0,
    'formstart' => 0,
    'filtertext' => 0,
    'prompt_category' => 0,
    'actionid' => 0,
    'categorylist' => 0,
    'curcategory' => 0,
    'prompt_showchildcategories' => 0,
    'allcategories' => 0,
    'prompt_sorting' => 0,
    'sortlist' => 0,
    'sortby' => 0,
    'prompt_pagelimit' => 0,
    'pagelimits' => 0,
    'formend' => 0,
    'addlink' => 0,
    'itemcount' => 0,
    'pagecount' => 0,
    'pagenumber' => 0,
    'form2start' => 0,
    'titletext' => 0,
    'postdatetext' => 0,
    'startdatetext' => 0,
    'enddatetext' => 0,
    'categorytext' => 0,
    'statustext' => 0,
    'items' => 0,
    'entry' => 0,
    'submit_massdelete' => 0,
    'categoryinput' => 0,
    'form2end' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_57d6c1f1704020_80596725',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57d6c1f1704020_80596725')) {function content_57d6c1f1704020_80596725($_smarty_tpl) {?><?php if (!is_callable('smarty_function_cms_help')) include '/var/www/html/cmsms-2.1.5-install/admin/plugins/function.cms_help.php';
if (!is_callable('smarty_function_html_options')) include '/var/www/html/cmsms-2.1.5-install/lib/smarty/plugins/function.html_options.php';
if (!is_callable('smarty_function_admin_icon')) include '/var/www/html/cmsms-2.1.5-install/admin/plugins/function.admin_icon.php';
if (!is_callable('smarty_function_form_start')) include '/var/www/html/cmsms-2.1.5-install/plugins/function.form_start.php';
if (!is_callable('smarty_function_cms_pageoptions')) include '/var/www/html/cmsms-2.1.5-install/plugins/function.cms_pageoptions.php';
if (!is_callable('smarty_function_form_end')) include '/var/www/html/cmsms-2.1.5-install/plugins/function.form_end.php';
if (!is_callable('smarty_modifier_cms_escape')) include '/var/www/html/cmsms-2.1.5-install/plugins/modifier.cms_escape.php';
if (!is_callable('smarty_modifier_cms_date_format')) include '/var/www/html/cmsms-2.1.5-install/plugins/modifier.cms_date_format.php';
?><script type="text/javascript">
//<![CDATA[
$(document).ready(function(){
	$('#selall').cmsms_checkall();
	$('#bulkactions').hide();
	$('#bulk_category').hide();
	$('#toggle_filter').click(function(){
	   $('#filter').dialog({
	     width: 'auto',
	     modal:  true
	   });
	});
        $('a.delete_article').click(function(){
        	return confirm('<?php echo strtr($_smarty_tpl->tpl_vars['mod']->value->Lang('areyousure'), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
        });
	$('#articlelist').on('cms_checkall_toggle','[type=checkbox]',function(){
		var l = $('#articlelist :checked').length;

		if( l == 0 ) {
			$('#bulkactions').hide(50);
		} else {
			$('#bulkactions').show(50);
		}
	});

	$('#bulk_action').on('change',function(){
		var v = $(this).val();

		if( v == 'setcategory' ) {
			$('#bulk_category').show(50);
		} else {
			$('#bulk_category').hide(50);
		}
	});

	$('#bulkactions').on('click','#submit_bulkaction',function(){
		return confirm('<?php echo strtr($_smarty_tpl->tpl_vars['mod']->value->Lang('areyousure_multiple'), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
	});
});
//]]>
</script>

<?php if (isset($_smarty_tpl->tpl_vars['formstart']->value)) {?>
<div id="filter" title="<?php echo $_smarty_tpl->tpl_vars['filtertext']->value;?>
" style="display: none;">
  <?php echo $_smarty_tpl->tpl_vars['formstart']->value;?>

  <div class="pageoverflow">
    <p class="pagetext"><label for="filter_category"><?php echo $_smarty_tpl->tpl_vars['prompt_category']->value;?>
:</label> <?php echo smarty_function_cms_help(array('key'=>'help_articles_filtercategory','title'=>$_smarty_tpl->tpl_vars['prompt_category']->value),$_smarty_tpl);?>
</p>
    <p class="pageinput">
      <select id="filter_category" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
category">
      <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['categorylist']->value,'selected'=>$_smarty_tpl->tpl_vars['curcategory']->value),$_smarty_tpl);?>

      </select>
      <label for="filter_allcategories"><?php echo $_smarty_tpl->tpl_vars['prompt_showchildcategories']->value;?>
:</label>
      <input id="filter_allcategories" type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
allcategories" value="yes" <?php if ($_smarty_tpl->tpl_vars['allcategories']->value=="yes") {?>checked="checked"<?php }?>>
      <?php echo smarty_function_cms_help(array('key'=>'help_articles_filterchildcats','title'=>$_smarty_tpl->tpl_vars['prompt_showchildcategories']->value),$_smarty_tpl);?>

    </p>
  </div>
  <div class="pageoverflow">
    <p class="pagetext"><label for="filter_sortby"><?php echo $_smarty_tpl->tpl_vars['prompt_sorting']->value;?>
:</label> <?php echo smarty_function_cms_help(array('key'=>'help_articles_sortby','title'=>$_smarty_tpl->tpl_vars['prompt_sorting']->value),$_smarty_tpl);?>
</p>
    <p class="pageinput">
      <select id="filter_sorting" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
sortby">
      <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['sortlist']->value,'selected'=>$_smarty_tpl->tpl_vars['sortby']->value),$_smarty_tpl);?>

      </select>
    </p>
  </div>
  <div class="pageoverflow">
    <p class="pagetext"><label for="filter_pagelimit"><?php echo $_smarty_tpl->tpl_vars['prompt_pagelimit']->value;?>
:</label> <?php echo smarty_function_cms_help(array('key'=>'help_articles_pagelimit','title'=>$_smarty_tpl->tpl_vars['prompt_pagelimit']->value),$_smarty_tpl);?>
</p>
    <p class="pageinput">
      <select id="filter_pagelimit" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
pagelimit">
      <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['pagelimits']->value,'selected'=>$_smarty_tpl->tpl_vars['sortby']->value),$_smarty_tpl);?>

      </select>
    </p>
  </div>
  <div class="pageoverflow">
    <p class="pageinput">
      <input type="submit" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
submitfilter" value="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('submit');?>
"/>
      <input type="submit" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
resetfilter" value="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('reset');?>
"/>
    </p>
  </div>
  <?php echo $_smarty_tpl->tpl_vars['formend']->value;?>

</div>
<?php }?>

<div class="row">
  <div class="pageoptions half" style="margin-top: 8px;">
    <a id="toggle_filter" <?php if ($_smarty_tpl->tpl_vars['curcategory']->value!='') {?> style="font-weight: bold; color: green;"<?php }?>><?php echo smarty_function_admin_icon(array('icon'=>'view.gif','alt'=>$_smarty_tpl->tpl_vars['mod']->value->Lang('viewfilter')),$_smarty_tpl);?>
 <?php if ($_smarty_tpl->tpl_vars['curcategory']->value!='') {?>*<?php }?><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('viewfilter');?>
</a>
    <?php if (isset($_smarty_tpl->tpl_vars['addlink']->value)) {?>&nbsp;<?php echo $_smarty_tpl->tpl_vars['addlink']->value;?>
<?php }?>
  </div>
</div>

<div style="clear:both"></div>

<?php if ($_smarty_tpl->tpl_vars['itemcount']->value>0) {?>
<div class="row">
  <?php if ($_smarty_tpl->tpl_vars['pagecount']->value>1) {?>
    <div class="pageoptions" style="text-align: right;">
      <?php echo smarty_function_form_start(array(),$_smarty_tpl);?>

      <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_page');?>
&nbsp;
      <select name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
pagenumber">
        <?php echo smarty_function_cms_pageoptions(array('numpages'=>$_smarty_tpl->tpl_vars['pagecount']->value,'curpage'=>$_smarty_tpl->tpl_vars['pagenumber']->value),$_smarty_tpl);?>

      </select>&nbsp;
      <input type="submit" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
paginate" value="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_go');?>
"/>
      <?php echo smarty_function_form_end(array(),$_smarty_tpl);?>

    </div>
  <?php }?>
</div>

<?php echo $_smarty_tpl->tpl_vars['form2start']->value;?>

<table class="pagetable" id="articlelist">
	<thead>
		<tr>
			<th>#</th>
			<th><?php echo $_smarty_tpl->tpl_vars['titletext']->value;?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['postdatetext']->value;?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['startdatetext']->value;?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['enddatetext']->value;?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['categorytext']->value;?>
</th>
			<th class="pageicon"><?php echo $_smarty_tpl->tpl_vars['statustext']->value;?>
</th>
			<th class="pageicon">&nbsp;</th>
			<th class="pageicon">&nbsp;</th>
			<th class="pageicon"><input type="checkbox" id="selall" value="1" title="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('selectall');?>
"/></th>
		</tr>
	</thead>
	<tbody>
	<?php  $_smarty_tpl->tpl_vars['entry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['entry']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['entry']->key => $_smarty_tpl->tpl_vars['entry']->value) {
$_smarty_tpl->tpl_vars['entry']->_loop = true;
?>
		<tr class="<?php echo $_smarty_tpl->tpl_vars['entry']->value->rowclass;?>
">
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->id;?>
</td>
			<td>
                        <?php if (isset($_smarty_tpl->tpl_vars['entry']->value->edit_url)) {?>
                          <a href="<?php echo $_smarty_tpl->tpl_vars['entry']->value->edit_url;?>
" title="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('editarticle');?>
"><?php echo smarty_modifier_cms_escape($_smarty_tpl->tpl_vars['entry']->value->news_title);?>
</a>
                        <?php } else { ?>
                          <?php echo smarty_modifier_cms_escape($_smarty_tpl->tpl_vars['entry']->value->news_title);?>

                        <?php }?>
                        </td>
			<td><?php echo smarty_modifier_cms_date_format($_smarty_tpl->tpl_vars['entry']->value->u_postdate);?>
</td>
                        <td><?php if (!empty($_smarty_tpl->tpl_vars['entry']->value->u_enddate)) {?><?php echo smarty_modifier_cms_date_format($_smarty_tpl->tpl_vars['entry']->value->u_startdate);?>
<?php }?></td>
                        <td><?php if ($_smarty_tpl->tpl_vars['entry']->value->expired==1) {?>
                              <div class="important">
                              <?php echo smarty_modifier_cms_date_format($_smarty_tpl->tpl_vars['entry']->value->u_enddate);?>

	                      </div>
                            <?php } else { ?>
                              <?php echo smarty_modifier_cms_date_format($_smarty_tpl->tpl_vars['entry']->value->u_enddate);?>

                            <?php }?>
                        </td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->category;?>
</td>
			<td><?php if (isset($_smarty_tpl->tpl_vars['entry']->value->approve_link)) {?><?php echo $_smarty_tpl->tpl_vars['entry']->value->approve_link;?>
<?php }?></td>
			<td>
                          <?php if (isset($_smarty_tpl->tpl_vars['entry']->value->edit_url)) {?>
                          <a href="<?php echo $_smarty_tpl->tpl_vars['entry']->value->edit_url;?>
" title="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('editarticle');?>
"><?php echo smarty_function_admin_icon(array('icon'=>'edit.gif'),$_smarty_tpl);?>
</a>
                          <?php }?>
                        </td>
			<td>
                          <?php if (isset($_smarty_tpl->tpl_vars['entry']->value->delete_url)) {?>
                          <a class="delete_article" href="<?php echo $_smarty_tpl->tpl_vars['entry']->value->delete_url;?>
" title="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('delete_article');?>
"><?php echo smarty_function_admin_icon(array('icon'=>'delete.gif'),$_smarty_tpl);?>
</a>
                          <?php }?>
                        </td>
			<td><input type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
sel[]" value="<?php echo $_smarty_tpl->tpl_vars['entry']->value->id;?>
" title="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('toggle_bulk');?>
"/></td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<?php } else { ?>
	<p class="warning"><?php if ($_smarty_tpl->tpl_vars['curcategory']->value=='') {?><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('noarticles');?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('noarticlesinfilter');?>
<?php }?></p>
<?php }?>

<div style="width: 99%;">
<?php if (isset($_smarty_tpl->tpl_vars['addlink']->value)) {?>
  <div class="pageoptions" style="float: left;">
    <p class="pageoptions"><?php echo $_smarty_tpl->tpl_vars['addlink']->value;?>
</p>
  </div>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['itemcount']->value>0) {?>
  <div class="pageoptions" style="float: right; text-align: right;" id="bulkactions">
    <label for="bulk_action"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('with_selected');?>
:</label>
    <select id="bulk_action" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
bulk_action">
    <?php if (isset($_smarty_tpl->tpl_vars['submit_massdelete']->value)) {?>
    <option value="delete"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('bulk_delete');?>
</option>
    <?php }?>
    <option value="setdraft"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('bulk_setdraft');?>
</option>
    <option value="setpublished"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('bulk_setpublished');?>
</option>
    <option value="setcategory"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('bulk_setcategory');?>
</option>
    </select>
    <div id="bulk_category" style="display: inline-block;">
      <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('category');?>
: <?php echo $_smarty_tpl->tpl_vars['categoryinput']->value;?>

    </div>
    <input type="submit" id="submit_bulkaction" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
submit_bulkaction" value="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('submit');?>
"/>
  </div>
<?php }?>
<div class="clearb"></div>
</div>
<?php echo $_smarty_tpl->tpl_vars['form2end']->value;?>

<?php }} ?>
