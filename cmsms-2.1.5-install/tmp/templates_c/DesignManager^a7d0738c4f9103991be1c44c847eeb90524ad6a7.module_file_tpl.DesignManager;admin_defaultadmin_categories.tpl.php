<?php /* Smarty version Smarty-3.1-DEV, created on 2016-09-12 17:33:40
         compiled from "module_file_tpl:DesignManager;admin_defaultadmin_categories.tpl" */ ?>
<?php /*%%SmartyHeaderCode:25123696957d6cad45de5b1-16332378%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a7d0738c4f9103991be1c44c847eeb90524ad6a7' => 
    array (
      0 => 'module_file_tpl:DesignManager;admin_defaultadmin_categories.tpl',
      1 => 1473692082,
      2 => 'module_file_tpl',
    ),
  ),
  'nocache_hash' => '25123696957d6cad45de5b1-16332378',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'list_categories' => 0,
    'mod' => 0,
    'url' => 0,
    'category' => 0,
    'rowclass' => 0,
    'edit_url' => 0,
    'delete_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1-DEV',
  'unifunc' => 'content_57d6cad4632d61_52018491',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57d6cad4632d61_52018491')) {function content_57d6cad4632d61_52018491($_smarty_tpl) {?><?php if (!is_callable('smarty_cms_function_cms_action_url')) include '/var/www/html/cmsms-2.1.5-install/plugins/function.cms_action_url.php';
if (!is_callable('smarty_function_admin_icon')) include '/var/www/html/cmsms-2.1.5-install/admin/plugins/function.admin_icon.php';
if (!is_callable('smarty_function_cycle')) include '/var/www/html/cmsms-2.1.5-install/lib/smarty/plugins/function.cycle.php';
?><?php if (isset($_smarty_tpl->tpl_vars['list_categories']->value)) {?>
<script type="text/javascript">
$(document).ready(function () {
    $('#categorylist tbody').cmsms_sortable_table({
        actionurl: '<?php echo smarty_cms_function_cms_action_url(array('action'=>'ajax_order_cats','forjs'=>1),$_smarty_tpl);?>
&showtemplate=false',
        callback: function(data) {

            var $response = $('<aside/>').addClass('message');

            if (data.status === 'success') {

                $response.addClass('pagemcontainer')
                    .append($('<span>').text('Close').addClass('close-warning'))
                    .append($('<p/>').text(data.message));
            } else if (data.status === 'error') {

                $response.addClass('pageerrorcontainer')
                    .append($('<span>').text('Close').addClass('close-warning'))
                    .append($('<p/>').text(data.message));
            }

            $('body').append($response).slideDown(1000, function() {
                window.setTimeout(function() {
                    $response.slideUp();
                    $response.remove();
                }, 10000);
            });
    	}
    });
});
</script>

<div class="pagewarning" style="display: block;"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('warning_category_dragdrop');?>
</div>
<?php }?>

<div class="pagecontainer">
	<?php echo smarty_cms_function_cms_action_url(array('action'=>'admin_edit_category','assign'=>'url'),$_smarty_tpl);?>

	<a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('create_category');?>
"><?php echo smarty_function_admin_icon(array('icon'=>'newobject.gif'),$_smarty_tpl);?>
</a>&nbsp;
	<a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('create_category');?>
"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('create_category');?>
</a>
</div>

<?php if (isset($_smarty_tpl->tpl_vars['list_categories']->value)) {?>
<table id="categorylist" class="pagetable">
	<thead>
		<tr>
			<th width="5%" title="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('title_cat_id');?>
"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_id');?>
</th>
			<th title="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('title_cat_name');?>
"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_name');?>
</th>
			<th class="pageicon"></th>
			<th class="pageicon"></th>
		</tr>
	</thead>
	<tbody>
	<?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
		<?php echo smarty_function_cycle(array('values'=>"row1,row2",'assign'=>'rowclass'),$_smarty_tpl);?>

		<?php echo smarty_cms_function_cms_action_url(array('action'=>'admin_edit_category','cat'=>$_smarty_tpl->tpl_vars['category']->value->get_id(),'assign'=>'edit_url'),$_smarty_tpl);?>

		<tr class="<?php echo $_smarty_tpl->tpl_vars['rowclass']->value;?>
 sortable-table" id="cat_<?php echo $_smarty_tpl->tpl_vars['category']->value->get_id();?>
">
			<td><a href="<?php echo $_smarty_tpl->tpl_vars['edit_url']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_edit');?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value->get_id();?>
</a></td>
			<td><a href="<?php echo $_smarty_tpl->tpl_vars['edit_url']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_edit');?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value->get_name();?>
</a></td>
			<td><a href="<?php echo $_smarty_tpl->tpl_vars['edit_url']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_edit');?>
"><?php echo smarty_function_admin_icon(array('icon'=>'edit.gif'),$_smarty_tpl);?>
</a></td>
			<td><?php echo smarty_cms_function_cms_action_url(array('action'=>'admin_delete_category','cat'=>$_smarty_tpl->tpl_vars['category']->value->get_id(),'assign'=>'delete_url'),$_smarty_tpl);?>
<a href="<?php echo $_smarty_tpl->tpl_vars['delete_url']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_delete');?>
" onclick="return confirm('<?php echo strtr($_smarty_tpl->tpl_vars['mod']->value->Lang('confirm_delete_category'), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');"><?php echo smarty_function_admin_icon(array('icon'=>'delete.gif'),$_smarty_tpl);?>
</a></td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<?php }?><?php }} ?>
