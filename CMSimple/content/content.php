<?php // utf8-marker = äöü
if(!defined('CMSIMPLE_VERSION') || preg_match('/content.php/i', $_SERVER['SCRIPT_NAME']))
{
	die('No direct access');
}
?>
<h1>Je ne sais paaaas</h1>
<p>Test de test haha</p>
<h1>Menu Levels &amp; Headings</h1>
<p>Headings h1–h3 are used for splitting up the content file. An h1, h2 or h3 heading will dynamically split the document into new pages in the table of contents. An h4, h5 or h6 heading is used as a heading within a page.</p>
<div style="text-align: center; font-weight: 700; background: #900; color: #ffffff; padding: 4px 6px;">
<p>Please use only h4–h6 in the content of your CMSimple pages, <br> because using h1–h3 will create a new page.</p>
</div>
<p>There is a plugin Pagemanager included in the download. Pagemanager simplifies creating, renaming, moving and deleting of pages.</p>
<p>Even complete folders with subfolders can be moved or deleted with Pagemanager.</p>
<p>Thus it should be no problem to get rid of the contents of this standard download. Alternatively you could start with the present page structure, rename pages and overwrite the contents.</p>
<h2>Menu Level 2 - Page 1</h2>
<p>An h2 heading splits into a new page.</p>
<h4>Heading h4</h4>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h5>Heading h5</h5>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h6>Heading h6</h6>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h3>Menu Level 3 - Page 1</h3>
<p>An h3 heading splits into a new page.</p>
<h4>Heading h4</h4>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h5>Heading h5</h5>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h6>Heading h6</h6>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h3>Menu Level 3 - Page 2</h3>
<p>An h3 heading splits into a new page.</p>
<h4>Heading h4</h4>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h5>Heading h5</h5>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h6>Heading h6</h6>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h3>Menu Level 3 - Page 3</h3>
<p>An h3 heading splits into a new page.</p>
<h4>Heading h4</h4>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h5>Heading h5</h5>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h6>Heading h6</h6>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h2>Menu Level 2 - Page 2</h2>
<p>An h2 heading splits into a new page.</p>
<h4>Heading h4</h4>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h5>Heading h5</h5>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h6>Heading h6</h6>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h3>Menu Level 3 - Page 1</h3>
<p>An h3 heading splits into a new page.</p>
<h4>Heading h4</h4>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h5>Heading h5</h5>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h6>Heading h6</h6>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h3>Menu Level 3 - Page 2</h3>
<p>An h3 heading splits into a new page.</p>
<h4>Heading h4</h4>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h5>Heading h5</h5>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h6>Heading h6</h6>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h3>Menu Level 3 - Page 3</h3>
<p>An h3 heading splits into a new page.</p>
<h4>Heading h4</h4>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h5>Heading h5</h5>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h6>Heading h6</h6>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h2>Menu Level 2 - Page 3</h2>
<p>An h2 heading splits into a new page.</p>
<h4>Heading h4</h4>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h5>Heading h5</h5>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h6>Heading h6</h6>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h3>Menu Level 3 - Page 1</h3>
<p>An h3 heading splits into a new page.</p>
<h4>Heading h4</h4>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h5>Heading h5</h5>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h6>Heading h6</h6>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h3>Menu Level 3 - Page 2</h3>
<p>An h3 heading splits into a new page.</p>
<h4>Heading h4</h4>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h5>Heading h5</h5>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h6>Heading h6</h6>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h3>Menu Level 3 - Page 3</h3>
<p>An h3 heading splits into a new page.</p>
<h4>Heading h4</h4>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h5>Heading h5</h5>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h6>Heading h6</h6>
<p>Headings h4–h6 do not split the content into new pages, but serve as normal headings to structure your text semantically.</p>
<h1>Templates &amp; Plugins</h1>
<p>Through the use of a different <strong>template</strong> you can give your pages a completely different look, and through <strong>plugins</strong> you can greatly enlarge the functionality of your CMSimple website.</p>
<div style="text-align: center; font-weight: 700; background: #900; color: #ffffff; padding: 4px 6px;">
<p>While installing templates or plugins, <br> please respect the licensing conditions of the authors.</p>
</div>
<p>Popular plugins provide picture galleries, guest books, comment possibilities, integration of video etc.</p>
<h1>Languages</h1>
<p>Please don’t delete the "default" and "en" language files (default.php and en.php), as they serve as default fallback.</p>
<h1>News01</h1>
<h4>Test de news</h4>
<p>On fais encore des test</p>
<p>On fais encore des test</p>
<p style="text-align: right;"><a href="http://www.cmsimple.org/doku/en/?Working_with_CMSimple___Newsboxes">cmsimple.org »</a></p>
<hr>
<p>Diese Box zeigt den Inhalt der versteckten Seite "News01".</p>
<p>Mehr Informationen zum Thema Newsboxen gibt es in der Dokumentation auf</p>
<p style="text-align: right;"><a href="http://www.cmsimple.org/doku/?Arbeiten_mit_CMSimple___Newsboxen">cmsimple.org »</a></p>
<h1>News02</h1>
<h4>Newsbox News02</h4>
<p>This box shows the content of the hidden page "News02".</p>
<p>More information about newsboxes can be found in the documentation on</p>
<p style="text-align: right;"><a href="http://www.cmsimple.org/doku/en/?Working_with_CMSimple___Newsboxes">cmsimple.org »</a></p>
<hr>
<p>Diese Box zeigt den Inhalt der versteckten Seite "News02".</p>
<p>Mehr Informationen zum Thema Newsboxen gibt es in der Dokumentation auf</p>
<p style="text-align: right;"><a href="http://www.cmsimple.org/doku/?Arbeiten_mit_CMSimple___Newsboxen">cmsimple.org »</a></p>
<h1>News03</h1>
<h4>Newsbox News03</h4>
<p>This box shows the content of the hidden page "News03".</p>
<p>More information about newsboxes can be found in the documentation on</p>
<p style="text-align: right;"><a href="http://www.cmsimple.org/doku/en/?Working_with_CMSimple___Newsboxes">cmsimple.org »</a></p>
<hr>
<p>Diese Box zeigt den Inhalt der versteckten Seite "News03".</p>
<p>Mehr Informationen zum Thema Newsboxen gibt es in der Dokumentation auf</p>
<p style="text-align: right;"><a href="http://www.cmsimple.org/doku/?Arbeiten_mit_CMSimple___Newsboxen">cmsimple.org »</a></p>
